﻿
using ado.net_lesson10.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_lesson10
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new UsersContext())
            {
                context.Users.Add(new User
                {
                    Login = "admin",
                    Password = "superadmin"
                });
                context.SaveChanges();
            }
        }
    }
}
