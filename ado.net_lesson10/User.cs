﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_lesson10.Properties
{ 
    [Table("_users")]

    public class User
    {
        //[Column("ID")]
        //[Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        //[Column("Log")]
        //[Required]
        //[StringLength(50)]
        public string Login { get; set; }
        //[Column("_pass")]
        //[Required]
        public string Password { get; set; }
    }
}
