namespace ado.net_lesson10
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using ado.net_lesson10.Properties;

    public partial class UsersContext : DbContext
    {
        public UsersContext()
            : base("UsersContext")
        {
        }

        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //������ ��������� ������ � �������
            modelBuilder
                 .Entity<User>()
                 .ToTable("_users");

            modelBuilder
                .Entity<User>()
                .Property(user => user.Id)
                .HasColumnName("ID")
                .IsRequired();


            modelBuilder
                .Entity<User>()
                .HasKey(user => user.Id);

            modelBuilder
               .Entity<User>()
               .Property(user=>user.Login)
                .HasColumnName("Log")
                .IsRequired();

            modelBuilder
              .Entity<User>()
              .Property(user => user.Password)
               .HasColumnName("_pass")
               .IsRequired();

            //modelBuilder
            //    .Entity<User>()
            //    .HasMany(user=>user.Orders)
            //    .WithRequired(order=>order.User)

        }

    }
}
